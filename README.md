## FEHRobotCode - Team B5 ##
## 2016 Fundamentals of Engineering for Honors at The Ohio State University ##

This code was used to autonomously control a robot to perform tasks on a course designed and built by the FEH program. QT Creator IDE was used to edit and compile the code. The code was executed on a custom-built [Proteus microcontroller](https://u.osu.edu/fehproteus/). The robot had 7 perfect runs and placed 2nd overall in the final competition. A video of the runs can be found at https://youtu.be/sdN_NM3KjPE

The robot project scenario was designed around the preparation tasks to launch a rocket.
The robot had to begin on a start light, correctly manipulate three launch sequence toggles, transfer an air purification system, navigate a ramp, press the correct fuel button, and press the final launch button within two minutes.