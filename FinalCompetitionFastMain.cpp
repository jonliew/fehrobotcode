// Required FEH libraries
#include <FEHLCD.h>
#include <FEHIO.h>

// Require custom libraries
#include "constants.h"
#include "worldstate.h"
#include "start.h"
#include "toggles_fast.h"
#include "supplies.h"
#include "ramp_fast.h"
#include "fuelbutton.h"
#include "launchbutton_fast.h"
#include "FinalCompetitionFastMain.h"

// Declare object FinalCompetition of class FinalCompetitionFastMain
FinalCompetitionFastMain FinalCompetitionFast;

/* This function adds a breakpoints to either continue or quit from the program.
 */
int FinalCompetitionFastMain::breakpoint() {
    if (false) {
        LCD.WriteRC("Press left button to", 5, 3);
        LCD.WriteRC("continue...", 6, 3);
        LCD.WriteRC("or middle to quit.", 7, 3);
        while(!button.LeftPressed() && !button.MiddlePressed());
        if (button.MiddlePressed()) {
            closeLog();
            return 1;
        }
        while(!button.LeftReleased() && !button.MiddleReleased());
        return 0;
    }
    return 0;
} // end breakpoint function

/* This function is a subprogram for the Final Competition that is run from the RobotMainMenu.
 * Last Modified: 4/6/2016 JKL
 */
void FinalCompetitionFastMain::Run() {

    start();

    togglesBottom_fast();

    if (FinalCompetitionFast.breakpoint())
        return;

    pickupSupplies();

    if (FinalCompetitionFast.breakpoint())
        return;

    tempRamp_fast();

    if (FinalCompetitionFast.breakpoint())
        return;

    fuelbutton();

    if (FinalCompetitionFast.breakpoint())
        return;

    dropOffSupplies();

    if (FinalCompetitionFast.breakpoint())
        return;

    togglesTop_fast();

    if (FinalCompetitionFast.breakpoint())
        return;

    mainRamp_fast();

    if (FinalCompetitionFast.breakpoint())
        return;

    launchButton_fast();

    return;
} // end FinalCompetitionMain::Run function
