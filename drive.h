#ifndef DRIVE_H
#define DRIVE_H
#include "constants.h"

void driveUntilTime(int heading, float power, int time, bool stop);
void driveWhileRotate(int startHeading, float turnDegree, float power, float turnPower, int time);
void driveUntilBump(int heading, float power, int bumpSide);
int driveUntilBumpTimeout(int heading, float power, int bumpSide, int timeout);
void driveUntilCds(int heading, float power, float cdsvalue, int timeout);
void driveUntilRPSx(float desiredX, float power, int faultHeading, int timeout);
void driveUntilRPSy(float desiredY, float power, int faultHeading, int timeout);
void driveUntilRPSxRange(float desiredX, float power, int faultHeading, float range, int timeout);
void driveUntilRPSyRange(float desiredY, float power, int faultHeading, float range, int timeout);

void turnUntilTime(float power, int time);
void turnUntilRPS(int desiredHeading, int power, int timeout);

#endif // DRIVE_H
