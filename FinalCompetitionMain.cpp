// Required FEH libraries
#include <FEHLCD.h>
#include <FEHIO.h>

// Require custom libraries
#include "constants.h"
#include "worldstate.h"
#include "start.h"
#include "toggles.h"
#include "supplies.h"
#include "ramp.h"
#include "fuelbutton.h"
#include "launchbutton.h"
#include "FinalCompetitionMain.h"

// Declare object FinalCompetition of class FinalCompetitionMain
FinalCompetitionMain FinalCompetition;

/* This function adds a breakpoints to either continue or quit from the program.
 */
int FinalCompetitionMain::breakpoint() {
    if (false) {
        LCD.WriteRC("Press left button to", 5, 3);
        LCD.WriteRC("continue...", 6, 3);
        LCD.WriteRC("or middle to quit.", 7, 3);
        while(!button.LeftPressed() && !button.MiddlePressed());
        if (button.MiddlePressed()) {
            closeLog();
            return 1;
        }
        while(!button.LeftReleased() && !button.MiddleReleased());
        return 0;
    }
    return 0;
} // end breakpoint function

/* This function is a subprogram for the Final Competition that is run from the RobotMainMenu.
 * Last Modified: 3/14/2016 JKL
 */
void FinalCompetitionMain::Run() {

    start();

    togglesBottom();

    if (FinalCompetition.breakpoint())
        return;

    pickupSupplies();

    if (FinalCompetition.breakpoint())
        return;

    tempRamp();

    if (FinalCompetition.breakpoint())
        return;

    fuelbutton();

    if (FinalCompetition.breakpoint())
        return;

    dropOffSupplies();

    if (FinalCompetition.breakpoint())
        return;

    togglesTop();

    if (FinalCompetition.breakpoint())
        return;

    mainRamp();

    if (FinalCompetition.breakpoint())
        return;

    launchButton();

    return;
} // end FinalCompetitionMain::Run function
