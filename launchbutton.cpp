// Required custom libraries
#include "constants.h"
#include "drive.h"
#include "worldstate.h"
#include "launchbutton.h"

/* This function drives the robot into the final launch button.
 * This function starts after the RSP y check after the robot goes down the main ramp
 * and ends after the robot hits the launch button.
 * Last modified: 4/2/2016 JKL
 */
void launchButton() {

    driveUntilTime(90, 100, 730, true);
    turnUntilTime(-60, 360);
    turnUntilRPS(225, 20, 2000);

    // The robot is given a 2x2 inch box it can correct its position to
    driveUntilRPSxRange( RPSFinalButtonX, 30, 45, 1, 1000);
    driveUntilRPSyRange( RPSFinalButtonY, 30, 45, 1, 1000);

    turnUntilRPS(225, 20, 2000);
    closeLog();
    driveUntilTime(90, 130, 3000, true);
} // end launchButton function
