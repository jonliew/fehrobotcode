// Required FEH libraries
#include <FEHRPS.h>

// Required custom libraries
#include "constants.h"
#include "drive.h"
#include "start.h"
#include "ramp.h"

/* This function drives the robot up the temporary access ramp.
 * This function starts after the RPS heading check at the lower entrance to the ramp
 * and ends after the RPS X check with the fuel light at the top of the ramp.
 * Last modified: 4/2/2016 JKL
 */
void tempRamp() {

    driveUntilBump(90, 120, 2);
    driveUntilBump(7, 120, 1);
    driveUntilBump(0, 50, 1);
    driveUntilBump(275, 70, -1);
    driveUntilTime(270, 60, 200, false);
    driveUntilRPSxRange( RPSTempRampTopX, 25, 270, 0.3, 3000);

    // if the robot is on the ramp still, keep driving until RPS is reacquired
    while (RPS.X() == -1) {
        driveUntilTime( 270, 90, 100, false);
    }

    driveUntilRPSxRange( RPSTempRampTopX, 25, 270, 0.3, 3000);
    turnUntilRPS(0, 20, 1000);
} // end tempRamp function

/* This function drives the robot down the main ramp.
 * This function starts at the blue toggle and ends after a heading check at the bottom of the main ramp.
 * Last modified: 4/2/2016 JKL
 */
void mainRamp() {

    driveUntilTime(270, 100, 1100, false);
    driveUntilRPSx( RPSTopRampX - RPSTopRampXOffset, 28, 0, 1000);
    driveUntilRPSyRange( RPSTopRampY, 28, 0, 0.4, 1000);
    turnUntilRPS(180, 20, 1000);

    // 358 heading to ensure that robot does not get caught on the weather station
    driveUntilTime(358, 50, 3200, true);

    driveUntilRPSyRange( RPSBottomRampY, 27, 0, 0.75, 4000);
    turnUntilRPS(180, 20, 1000);
} // end mainRamp function
