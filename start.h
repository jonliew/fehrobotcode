#ifndef START_H
#define START_H

void start();

// extern these values so that other .cpp files can use the RPS offset values
extern float RPSSuppliesYOffset;
extern float RPSFuelLightXOffset;
extern float RPSFuelLightYOffset;
extern float RPSTopRampXOffset;

#endif // START_H
