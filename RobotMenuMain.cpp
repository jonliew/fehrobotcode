// Required libraries
#include <FEHLCD.h>
#include <string.h>
#include "RobotMenuMain.h"

// Included custom libraries. Comment out the ones not needed.
// Be sure to comment out the functions in the code farther down in RobotMenuMain::Run()
//#include "ProteusTestMain.h"
#include "WorldStateMain.h"
#include "FinalCompetitionMain.h"
#include "FinalCompetitionFastMain.h"

// Define the colors for the menus
#define MENU_C SCARLET
#define TEXT_C BLACK

// Declare the object of type RobotMenuMain
RobotMenuMain RobotMenu;

/* This function draws the main menu, accepts input, requests for confirmation, and returns the choice.
 * This code was mostly inspired by Proteus_Test.cpp and FEHRPS.cpp.
 * Last Modified: 3/14/2016 JKL
 */
int RobotMenuMain::MNMenu(void)
{
    // Initialize/reset choice and menu
    int confirmChoice = 0;
    int menu = 0;

    // Initialize the main menu title, main menu labels, confirmation title, and confirmation menu labels.
    FEHIcon::Icon MAIN_T[1];
    char main_t_label[1][20] = {"Proteus Main Menu"};

    FEHIcon::Icon MAIN[6];
    char main_label[6][20] = {"Proteus Test","WorldState","Blank","FinalComp","Test1","FinalFast"};

    FEHIcon::Icon confirm_title[1];
    char confirm_title_label[1][20] = {""};

    FEHIcon::Icon confirm[2];
    char confirm_labels[2][20] = {"Ok", "Cancel"};

    while (confirmChoice != 1) {
        LCD.Clear(GRAY);

        // Draw the title block
        FEHIcon::DrawIconArray(MAIN_T, 1, 1, 1, 201, 1, 1, main_t_label, MENU_C, TEXT_C);
        MAIN_T[0].Select();

        // Draw the menu options with 3 rows and 2 columns
        FEHIcon::DrawIconArray(MAIN, 3, 2, 40, 1, 1, 1, main_label, MENU_C, TEXT_C);

        float x, y;

        menu = 0;

        while(menu==0) {
            if (LCD.Touch(&x, &y)) {
                for (int n=0; n<=9; n++) {
                    if (MAIN[n].Pressed(x, y, 0)) {
                        menu = n + 1;
                        MAIN[n].WhilePressed(x, y);
                        break;
                    }
                }
            }
        } // end while loop that repeats until a icon is pressed

        strcpy(confirm_title_label[0], main_label[menu-1]);

        LCD.Clear(GRAY);
        FEHIcon::DrawIconArray(confirm_title, 1, 1, 60, 201, 1, 1, confirm_title_label, MENU_C, TEXT_C);
        FEHIcon::DrawIconArray(confirm, 1, 2, 60, 60, 1, 1, confirm_labels, MENU_C, TEXT_C);

        confirmChoice = 0;

        while (!confirmChoice) {
            if (LCD.Touch(&x, &y)) {
                for (int n=0; n <=1; n++) {
                    if (confirm[n].Pressed(x, y, 0)) {
                        confirm[n].WhilePressed(x, y);
                        confirmChoice = n+1;
                    }
                }
            }
        } // end while loop that repeats until a confirmation choice
    } // end while loop the repeats until positive confirmation

    return menu;
} // end RobotMenuMain::MNMenu function

/* This function is the main function that runs in main.cpp.
 * It contains the switch-case options for the menu choices and runs the included programs.
 * Be sure to comment out the subprograms not included at the top of this file.
 * Last Modified: 3/14/2016 JKL
 */
void RobotMenuMain::Run() {
    int choice = MNMenu();
    LCD.Clear(BLACK);
    switch (choice) {
    case 1:
        LCD.WriteLine("Proteus Test");
        //ProteusTest.Run();
        break;
    case 2:
        LCD.WriteLine("WorldState");
        WorldState.Run();
        break;
    case 3:
        LCD.WriteLine("Blank");
        break;
    case 4:
        LCD.WriteLine("Final Competition");
        FinalCompetition.Run();
        break;
    case 5:
        LCD.WriteLine("Test1");
        break;
    case 6:
        LCD.WriteLine("Final Fast");
        FinalCompetitionFast.Run();
        break;

    } // end switch-case structure
} // end RobotMenuMain::Run function
