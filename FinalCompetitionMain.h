#ifndef FINALCOMPETITIONMAIN_H
#define FINALCOMPETITIONMAIN_H

class FinalCompetitionMain {
public:
    void Run();
private:
    int breakpoint();
};

extern FinalCompetitionMain FinalCompetition;

#endif // FINALCOMPETITIONMAIN_H
