#ifndef WORLDSTATE_H
#define WORLDSTATE_H

void initializeLog();
void worldState(bool updateLog, int heading, float power, float motor1ratio, float motor2ratio, float motor3ratio, float motor4ratio);
void closeLog();

#endif // WORLDSTATE_H
