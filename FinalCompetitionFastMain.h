#ifndef FINALCOMPETITIONFASTMAIN_H
#define FINALCOMPETITIONFASTMAIN_H

class FinalCompetitionFastMain {
public:
    void Run();
private:
    int breakpoint();
};

extern FinalCompetitionFastMain FinalCompetitionFast;

#endif // FINALCOMPETITIONFASTMAIN_H
