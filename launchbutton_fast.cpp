// Required custom libraries
#include "constants.h"
#include "drive.h"
#include "worldstate.h"
#include "launchbutton_fast.h"

/* This function drives the robot into the final launch button.
 * This function starts after the RSP y check after the robot goes down the main ramp
 * and ends after the robot hits the launch button.
 * Last modified: 4/2/2016 JKL
 */
void launchButton_fast() {

    driveUntilTime(90, 120, 600, true);
    turnUntilTime(-60, 360);
    turnUntilRPS(225, 20, 2000);
    driveUntilRPSxRange( RPSFinalButtonX, 30, 45, 1, 1000);
    driveUntilRPSyRange( RPSFinalButtonY, 30, 45, 1, 1000);
    closeLog();
    driveUntilTime(90, 130, 3000, true);
} // end launchButton function
