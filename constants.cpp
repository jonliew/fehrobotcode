// Required FEH libraries
#include <FEHIO.h>
#include <FEHMotor.h>
#include <FEHServo.h>

// Required custom library
#include "constants.h"

// Declare the four drive motors going in a clockwise direction
FEHMotor motor1(FEHMotor::Motor0, 7.2);
FEHMotor motor2(FEHMotor::Motor1, 7.2);
FEHMotor motor3(FEHMotor::Motor2, 7.2);
FEHMotor motor4(FEHMotor::Motor3, 7.2);

// Declare the servo for the arm for the supplies
FEHServo arm_servo(FEHServo::Servo0);

// Declare the microswitches going in a clockwise direction
DigitalInputPin microswitch1(FEHIO::P1_0);
DigitalInputPin microswitch2(FEHIO::P1_2);
DigitalInputPin microswitch3(FEHIO::P1_4);
DigitalInputPin microswitch4(FEHIO::P1_6);
DigitalInputPin microswitch5(FEHIO::P2_0);
DigitalInputPin microswitch6(FEHIO::P2_2);
DigitalInputPin microswitch7(FEHIO::P2_4);
DigitalInputPin microswitch8(FEHIO::P2_6);

// Declare other sensors and the ButtonBoard
AnalogInputPin cdscell(FEHIO::P0_0);
ButtonBoard button(FEHIO::Bank3);
