#ifndef WORLDSTATEMAIN_H
#define WORLDSTATEMAIN_H

class WorldStateMain {
public:
    void Run();
};

extern WorldStateMain WorldState;

#endif // WORLDSTATEMAIN_H
