// Required FEH libraries
#include <FEHIO.h>
#include <FEHLCD.h>

// Required custom libraries
#include "drive.h"
#include "supplyarm.h"
#include "start.h"
#include "constants.h"
#include "supplies.h"

/* This function makes the robot align with and pick up the supplies.
 * This function starts after the robot backs up from the blue toggle
 * and ends after aligning with the lower entrance if the temporary access ramp.
 * Last modified: 4/2/2016 JKL
 */
void pickupSupplies() {

    driveUntilTime(45, 90, 1450, true);
    driveUntilRPSyRange(11.9, 30, 0, 1.5, 4000); // CHECK THIS OUT!
    turnUntilTime(-70, 550);
    turnUntilRPS(0,20, 2000);

    driveUntilTime(90, 100, 600, false);
    driveUntilBump(90, 60, 2);

    driveUntilRPSy( RPSSuppliesY - RPSSuppliesYOffset, 27, 180, 8000);

    // Failsafe code
    // If the robot gets caught on the supply box and the bumpSide is not triggered,
    // drive away and retry alignment with the wall.
    if ( driveUntilBumpTimeout(90, 30, 2, 5000) == 1 ) {
        driveUntilTime( 0, 40, 500, true);
        driveUntilBump(90, 60, 2);
        driveUntilRPSy( RPSSuppliesY - RPSSuppliesYOffset, 27, 180, 8000);
        driveUntilBumpTimeout( 90, 30, 2, 2000);
    }

    lowerToPickupArm();
    raiseToPickupArm();

    driveUntilTime(90, 30, 200, true);
    driveUntilBump( 9,80,-2);

    driveUntilRPSy( RPSTempRampBottomY, 27, 270, 4000);
    turnUntilRPS(0, 20, 1000);
} // end pickupSupplies function

/* This function makes the robot drop off the supplies at the drop zone.
 * This function starts after a RPS heading check after the fuel button
 * and ends after driving away from the drop zone.
 * Last modified: 4/2/2016 JKL
 */
void dropOffSupplies() {

    driveUntilTime(88, 140, 950, false);
    driveUntilBump(90, 60, 2);

    // Drive with heading 180 until microswitch 6 is bumped
    while(microswitch6.Value()) {
        motor1.SetPercent(-20);
        motor2.SetPercent(20);
        motor3.SetPercent(20);
        motor4.SetPercent(-20);
    }
    motor1.SetPercent(0);
    motor2.SetPercent(0);
    motor3.SetPercent(0);
    motor4.SetPercent(0);

    driveUntilTime(270, 50, 150, true);

    lowerToDepositArm();
    driveUntilTime(350, 80, 550, true);
    raiseToDepositArm();
} // end dropOffSupplies function
