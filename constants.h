#ifndef CONSTANTS_H
#define CONSTANTS_H
#include <FEHMotor.h>
#include <FEHIO.h>
#include <FEHServo.h>

#define PI 3.14159

// This information is logged in the SD log files
// VERSION is [major build].[month][day]
// TITLE is the program name
#define VERSION 3.0409
#define TITLE "FEHRobotCode"

// Define the min and max for the arm servo
#define SERVO_MIN 606
#define SERVO_MAX 2016

// Define CdS cell values
#define CDSRedLow      0.0
#define CDSRedHigh     0.8
#define CDSBlueLow     1.0
#define CDSBlueHigh    2.6

// Define all RPS values
#define RPSOffStart         24.0
#define RPSSuppliesY        10.8 // calibrate per course
#define RPSTempRampBottomY  24.0
#define RPSTempRampTopX     31.3
#define RPSFuelLightY       61.8 // calibrate per course
#define RPSFuelLightX       31.8 // calibrate per course
#define RPSLongRunY         50.0
#define RPSRedTopY          43.5
#define RPSRedTopX           1.5
#define RPSWhiteTopX         6.4
#define RPSBlueTopX         11.5
#define RPSTopRampX         27.7 // calibrate per course
#define RPSTopRampY         42.5
#define RPSBottomRampY      21.3
#define RPSFinalButtonX     18.4
#define RPSFinalButtonY     21.3

// Use of extern so that these can be used elsewhere in the code
extern FEHMotor motor1;
extern FEHMotor motor2;
extern FEHMotor motor3;
extern FEHMotor motor4;

extern FEHServo arm_servo;

extern DigitalInputPin microswitch1;
extern DigitalInputPin microswitch2;
extern DigitalInputPin microswitch3;
extern DigitalInputPin microswitch4;
extern DigitalInputPin microswitch5;
extern DigitalInputPin microswitch6;
extern DigitalInputPin microswitch7;
extern DigitalInputPin microswitch8;

extern AnalogInputPin cdscell;
extern ButtonBoard button;

#endif // CONSTANTS_H
