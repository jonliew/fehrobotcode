// Required FEH libraries
#include <FEHLCD.h>
#include <FEHIO.h>
#include <FEHUtility.h>
#include <FEHRPS.h>

// Required custom libraries
#include "constants.h"
#include "worldstate.h"
#include "WorldStateMain.h"

// Declare object WorldState of class WorldStateMain
WorldStateMain WorldState;

/* This function is a subprogram executed from the RobotMainMenu that continuously displays the world state of the robot.
 * Last modified: 4/2/2016
 */
void WorldStateMain::Run()
{
    RPS.InitializeTouchMenu();
    initializeLog();

    LCD.WriteLine("Waiting for middle");
    while( !button.MiddlePressed() );

    worldState(false, 0, 0, 0, 0, 0, 0);
    LCD.WriteRC("Right button - log", 5, 3);
    LCD.WriteRC("Left button - quit", 6, 3);

    while( !button.LeftPressed() ) {  
        worldState(false, 0,0,0,0,0,0);

        // Write to SD card file if right button is pressed
        if ( button.RightPressed() ) {
            worldState(true, 0, 0, 0, 0, 0, 0);
            LCD.WriteRC("Logged", 5, 3);
        }
        Sleep(50);
    }
    closeLog();
    LCD.WriteRC("SD Log Closed", 6, 3);
} // end WorldStateMain::Run function
