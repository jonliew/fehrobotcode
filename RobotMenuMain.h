#ifndef ROBOTMAINMENU_H
#define ROBOTMAINMENU_H

class RobotMenuMain {
public:
    void Run();
private:
    int MNMenu();
};

extern RobotMenuMain RobotMenu;

#endif // ROBOTMAINMENU_H
