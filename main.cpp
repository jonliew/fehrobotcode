// Required custom library
#include "RobotMenuMain.h"

/* This main function runs the RobotMainMenu, which can access all of the subprograms.
 * Last modified: 3/14/2016
 */
int main(void)
{
    while(true) {
        RobotMenu.Run();
    }
} // end main function
