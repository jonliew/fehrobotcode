/* This is part of a custom header that contains the functions to operate the servo
 * for the arm to pickup and deposit the supplies.
 * Last modified: 3/21/2016 JKL
 */

// Required FEH libraries
#include <FEHServo.h>
#include <FEHUtility.h>

// Required custom libraries
#include "constants.h"
#include "worldstate.h"
#include "supplyarm.h"

/* This function is run from void start() at the beginning of the run.
 * It sets the minimum and maximum of the servo and raises it to an upright position.
 * Last modified: 3/21/2016 JKL
 */
void initializeArm() {
    arm_servo.SetMin(SERVO_MIN);
    arm_servo.SetMax(SERVO_MAX);
    arm_servo.SetDegree(175);
}

/* This function is run to lower the arm to contact the supplies.
 * Last modified: 3/21/2016 JKL
 */
void lowerToPickupArm() {
    arm_servo.SetDegree(16);
    Sleep(1000);
}

/* This function is run to pickup the supplies without dropping it.
 * A delay must be added to ensure that the supplies are picked up.
 * Last modified: 3/21/2016 JKL
 */
void raiseToPickupArm() {
    for (int degree = 18; degree <= 175; degree+= 2 ) {
        arm_servo.SetDegree(degree);
        Sleep(10);
    }
}

/* This function is run to lower the supplies into the drop zone.
 * Last modified: 4/2/2016 JKL
 */
void lowerToDepositArm() {
    arm_servo.SetDegree(63);
    Sleep(400);
}

/* This function is run to raise the arm after the supplies are deposited.
 * Last modified: 3/21/2016 JKL
 */
void raiseToDepositArm() {
    arm_servo.SetDegree(160);
}
