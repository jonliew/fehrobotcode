#ifndef SUPPLYARM_H
#define SUPPLYARM_H

void initializeArm();
void lowerToPickupArm();
void raiseToPickupArm();
void lowerToDepositArm();
void raiseToDepositArm();

#endif // SUPPLYARM_H
