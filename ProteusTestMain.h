#ifndef PROTEUSTEST_H
#define PROTEUSTEST_H

#define PROTEUSTESTMAIN 1

class ProteusTestMain {
public:
    void Run();
};

extern ProteusTestMain ProteusTest;

#endif // PROTEUSTEST_H
